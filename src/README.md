<!-- #here  is an README file that provide instructions for for porjec t setups  -->

# Task Management System

## Overview
This Django project implements a simple Task Management System with RESTful APIs using Django REST Framework.

## Setup

### Prerequisites
- Python (3.10)
- Django (4)
- Django REST Framework
- Django Filters
- Rest Framework Simple JWT
- Django Spectacular

### Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/ismail2395333/task_management_system_review_task.git
   

# Create a virtual environment (optional but recommended):

python -m venv venv
source venv/bin/activate   # On Windows: .\venv\Scripts\activate

# Install dependencies:

pip install -r requirements.txt

# Run migrations:

python manage.py migrate

# Create a superuser (for accessing Django admin):

python manage.py createsuperuser

# Run the development server:

python manage.py runserver

# The project will be accessible at http://127.0.0.1:8000/

# Documentation

API documentation can be accessed at http://127.0.0.1:8000/api/schema/swagger-ui/

APIs
Tasks API Endpoints
List Tasks:

Endpoint: /tasks/
Method: GET
Create Task:

Endpoint: /tasks/create/
Method: POST
Retrieve Task:

Endpoint: /tasks/{task_id}/
Method: GET
Update Task:

Endpoint: /tasks/update/{task_id}/
Method: PUT
Delete Task:

Endpoint: /tasks/delete/{task_id}/
Method: DELETE

Authentication
The API uses JWT (JSON Web Token) authentication.

# Obtain Token to gain the access of task app 
Endpoint: /gettoken/
Method: POST
Parameters: username, password

# Refresh Token
Endpoint: /refreshtoken/
Method: POST
Parameters: refresh

# Verify Token
Endpoint: /verifytoken/
Method: POST
Parameters: token


# Testing

# To run tests, use the following command:

python manage.py test




