from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from .models import Task

class TaskAPITestCase(TestCase):
    def setUp(self):
        # Create a test user for authentication
        self.user = User.objects.create_user(username='testuser', password='testpassword')

        # Create tasks for testing
        self.task1 = Task.objects.create(title='Task 1', description='Description 1', status='Incomplete', owner=self.user)
        self.task2 = Task.objects.create(title='Task 2', description='Description 2', status='Complete', owner=self.user)
        self.task3 = Task.objects.create(title='Task 3', description='Description 3', status='Incomplete')

        # Create an API client for making requests
        self.client = APIClient()

    def test_list_tasks(self):
        # Test listing tasks
        response = self.client.get('/tasks/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)  # User should see only their tasks

    def test_create_task(self):
        # Test creating a new task
        data = {'title': 'New Task', 'description': 'New Description', 'status': 'Incomplete'}
        self.client.force_authenticate(user=self.user)
        response = self.client.post('/tasks/create/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Task.objects.count(), 4)  # Check that a new task was created

    def test_retrieve_task(self):
        # Test retrieving details of a task
        response = self.client.get(f'/tasks/{self.task1.id}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], 'Task 1')

    def test_update_task(self):
        # Test updating an existing task
        data = {'title': 'Updated Task', 'status': 'Complete'}
        self.client.force_authenticate(user=self.user)
        response = self.client.put(f'/tasks/update/{self.task2.id}/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.task2.refresh_from_db()
        self.assertEqual(self.task2.title, 'Updated Task')

    def test_delete_task(self):
        # Test deleting an existing task
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(f'/tasks/delete/{self.task3.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Task.objects.filter(id=self.task3.id).exists())  # Check that the task was deleted


# python manage.py test to check this test.py use this on terminal
