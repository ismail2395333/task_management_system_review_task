from rest_framework import permissions

class IsTaskOwnerOrAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        # Allow admin users to perform any action
        if request.user and request.user.is_staff:
            return True

        # Allow regular users to perform actions only on their own tasks
        return obj.owner == request.user
