from django.contrib import admin
from .models import Task

@admin.register(Task)
class Taskadmin(admin.ModelAdmin):
    list_display = ('title','description','status', 'created_at', 'updated_at')


