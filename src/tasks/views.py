from django.shortcuts import render
from rest_framework import generics
from tasks.models import Task
from tasks.serializers import TaskSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.pagination import PageNumberPagination
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated
from .permissions import IsTaskOwnerOrAdmin


# Note was about to create listcreate and retriveupdatedelete Together but created seprately

class TaskCreateView(generics.CreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, IsTaskOwnerOrAdmin]

class TaskRetrieveView(generics.RetrieveAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, IsTaskOwnerOrAdmin]

class TaskUpdateView(generics.UpdateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, IsTaskOwnerOrAdmin]

class TaskDestroyView(generics.DestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, IsTaskOwnerOrAdmin]

class TaskListView(generics.ListAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ['status']  # Fields for filtering
    search_fields = ['title', 'description']  # Fields for searching
    permission_classes = [IsAuthenticated, IsTaskOwnerOrAdmin]
    # pagination_class = MyPageNumberPagination  # Use the custom pagination class

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            # Admin user can see all tasks
            return Task.objects.all()
        else:
            # Normal user can see only their own tasks
            return Task.objects.filter(owner=user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)



# implemnted in setting global pagination so commenting this custom pagination or ve can create sprate file .
# class MyPageNumberPagination(PageNumberPagination):
#    page_size = 1
#    page_query_param = 'p' # pagenumber pagination we use ?page=2 now we can use ?p=2
#    page_size_query_param = 'records' # here we can give how muc record we can define how much we can show data to perticular page .
#    max_page_size = 7  # cleint can seee 7 record only on single page 
#    last_page_strings = ("end", ) # suppose if we had write last now we can write end as we had change string

