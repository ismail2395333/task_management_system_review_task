from django.db import models
from django.contrib.auth.models import User

class Status(models.TextChoices):
    COMPLETE = 'Complete', 'Complete'
    INCOMPLETE = 'Incomplete', 'Incomplete'
    
class Task(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=12, choices=Status.choices)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tasks_created')  # Added owner field
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'task'
        verbose_name_plural = 'tasks'
        db_table = 'tasks'

    def __str__(self):
        return str(self.title)
